package facci.adrianagilces.conversionapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ConversionCaF extends AppCompatActivity {

    private EditText valorC;
    private TextView resultado,resultText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_ca_f);

        valorC = findViewById(R.id.convCaF);
        resultado = findViewById(R.id.result_CaF);
        resultText = findViewById(R.id.id_result);
    }

    public void conversionCaF(View view){

        String ingreso = valorC.getText().toString();

        float ingresoFloat = Float.parseFloat(ingreso);

        float convertir = (1.8f * ingresoFloat) + 32;

        double recort = Math.round(convertir * 100.0) / 100.0;

        String result = Double.toString(recort);

        String mostrarResultado = String.format("%s °F", result);

        resultText.setText(R.string.resultado);
        resultado.setText(mostrarResultado);


    }
}