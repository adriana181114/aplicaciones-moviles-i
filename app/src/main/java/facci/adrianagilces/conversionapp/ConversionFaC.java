package facci.adrianagilces.conversionapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ConversionFaC extends AppCompatActivity {

    private EditText valorF;
    private TextView resultado2,resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion_fa_c);

        valorF = findViewById(R.id.convFaC);
        resultado2 = findViewById(R.id.result_FaC);
        resultText = findViewById(R.id.id_result2);
    }

    public void conversionFaC(View view){
        String ingreso = valorF.getText().toString();

        float ingresoFloat = Float.parseFloat(ingreso);

        float convertir = (ingresoFloat - 32) / 1.8f;

        double recort = Math.round(convertir * 100.0) / 100.0;

        String result = Double.toString(recort);

        String mostrarResultado = String.format("%s °C", result);

        resultText.setText(R.string.resultado);
        resultado2.setText(mostrarResultado);
    }
}