package facci.adrianagilces.conversionapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainActivity","AdrianaCristinaGilcesPeñafiel");
        setContentView(R.layout.activity_main);
    }
    public void siguienteConvertirCaF(View view){
        Intent siguiente = new Intent(this, ConversionCaF.class);
        startActivity(siguiente);
    }

    public void siguienteConvertirFaC(View view){
        Intent siguiente = new Intent(this, ConversionFaC.class);
        startActivity(siguiente);
    }
}